import javax.swing.*;

/**
 * @author TheNameMan
 */
public class Main {

    public static void main(String[] args) {
        /*String amount = JOptionPane.showInputDialog("Amount of Seeds", "1");
        int am = 1;
        try {
            am = Integer.parseInt(amount);
            if(am > 20) {
                am = 20; //cap the amount of seeds to 20
            }
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Error: Your input is not a number.", "Input Error", JOptionPane.ERROR_MESSAGE);
            return;
        }*/
        Object seed = randomSeed();
        /*for(int i = 0; i < am; i++) {
            Object seed = randomSeed();
            seeds = seeds + (seeds.equals("") ? "" : "\n") + seed;
        }*/
        JOptionPane.showInputDialog("Seed:", seed);
        int a = JOptionPane.showConfirmDialog(null, "Generate another?", "Seed Generator", JOptionPane.YES_NO_OPTION);
        while(a == 0) {
            seed = randomSeed();
            JOptionPane.showInputDialog("Seed:", seed);
            a = JOptionPane.showConfirmDialog(null, "Generate another?", "Seed Generator", JOptionPane.YES_NO_OPTION);
        }
    }

    public static Object randomSeed() {
        String seed = "";
        boolean minus = randomBoolean();
        //now generate the seed (19 characters without a minus, 20 characters with)
        for(int i = 0; i < 19; i++) {
            if(minus && seed.equals("")) {
                seed = "-";
            }
            seed = seed + Math.round(Math.random() * 9);
        }
        return seed;
    }

    private static boolean randomBoolean() {
        double i = (int) Math.round(Math.random() * 12);
        if(i == 1) return true;
        if(i == 2) return true;
        if(i == 3) return true;
        if(i == 4) return true;
        if(i == 5) return true;
        if(i == 6) return true;
        else return false;
    }

}